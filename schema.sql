create database if not exists marketsdb;
use marketsdb;
create table if not exists markets (
 ticker char(12) not null,
 primary key (ticker)
);
-- insert sample data
insert into markets values ('FTSE');
insert into markets values ('NYSE');
insert into markets values ('NASDAQ');
